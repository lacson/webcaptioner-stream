FROM python:3

WORKDIR /usr/src/app

# generate certs
COPY openssl.cnf ./
RUN openssl req -x509 -config openssl.cnf -newkey rsa:4096 -nodes -out cert.pem -keyout key.pem 

# install our requirements
COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt

# copy script in
COPY ./stream.py .

# declare the port to use and pass as env var
ARG port=9999
ENV FLASK_PORT=${port}
EXPOSE ${port}:${port}

# mark entrypoint
ENTRYPOINT ["python3", "stream.py"]
CMD []
